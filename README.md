# Vietnamese language models for Spacy.io 
## vi_core_news_md
(TBA)
## vi_core_news_lg
Spacy Vietnamese language model trained on 18 GB of Vietnamese news, 270K unique vectors (300 dimensions)
Pipeline: pretrained word vectors, tok2vec, tagger, parser
| Metrics   |      Score    |
|----------|:-------------:|
| sents_p |  0.9040097205 |
| sents_r |    0.93   |
| tag_acc | 0.8942170893 |
| dep_uas | 0.7110308826 |
| dep_las | 0.6303722133 |
| sents_f | 0.9168207024 |
# Installation
pip install https://gitlab.com/trungtv/vi_spacy/-/raw/master/vi_core_news_lg/dist/vi_core_news_lg-0.0.1.tar.gz
# Usage
## Without pretrained pipeline
```
from spacy.lang.vi import Vietnamese
nlp = Vietnamese()
doc = nlp('Bách Khoa Hà Nội')
```
## Pretrained pipelines (e.g. vi_core_news_lg)
```
import spacy
nlp = spacy.load('vi_core_news_lg')
doc = nlp('Cộng đồng xử lý ngôn ngữ tự nhiên')
for token in doc:
    print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
            token.shape_, token.is_alpha, token.is_stop)
```
